%bcond_with hadoop
%bcond_without ftp
%bcond_without ssh

Name:           apache-commons-vfs
Version:        2.1
Release:        16
Summary:        Commons Virtual File System
License:        ASL 2.0
Url:            https://commons.apache.org/proper/commons-vfs/
Source0:        https://archive.apache.org/dist/commons/vfs/source/commons-vfs-%{version}-src.tar.gz
Patch0001:      fix-wrong-value-returned-when-read-0xff.patch
BuildRequires:  maven-local mvn(commons-httpclient:commons-httpclient) mvn(commons-logging:commons-logging)
BuildRequires:  mvn(commons-net:commons-net) mvn(org.apache.ant:ant) mvn(org.apache.commons:commons-collections4)
BuildRequires:  mvn(org.apache.commons:commons-compress) mvn(org.apache.commons:commons-parent:pom:)
BuildRequires:  mvn(org.apache.maven.plugins:maven-antrun-plugin)
%if %{with hadoop}
BuildRequires:  mvn(org.apache.hadoop:hadoop-common) mvn(org.apache.hadoop:hadoop-hdfs)
%endif
%if %{with ssh}
BuildRequires:  mvn(com.jcraft:jsch)
%endif
%if %{with ftp}
BuildRequires:  mvn(org.apache.ftpserver:ftpserver-core)
%endif

BuildArch:      noarch
Provides:       %{name}2 = %{version}-%{release} %{name}-project = %{version}-%{release}
Obsoletes:      %{name}-project < %{version}-%{release}

%description
Commons VFS provides a uniform view of files through a single API which is
designed for accessing various different file systems. These file systems could
be a local disk, an HTTP server or a ZIP archive file.
The key features are listed as follows:
* The API is consistent among various file types.
* Support for a wide range of file systems.
* Support caching local file system with different fs types.
* Event delivery.
* Provides in-JVM info caching.
* A set of Ant tasks which VFS is enabled.
* Easy to be intergrated into applications such as VFS-aware ClassLoader and URLStreamHandlerFactory.

%package        devel
Summary:        Development files for Commons VFS
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-ant = %{version}-%{release} %{name}-examples = %{version}-%{release}
Obsoletes:      %{name}-ant < %{version}-%{release} %{name}-examples < %{version}-%{release}

%description    devel
Development files for Commons VFS. These files enable support for
the Commons VFS ant tasks and provides some VFS examples.

%package        help
Summary:        Javadoc for %{name}
Provides:       %{name}-javadoc = %{version}-%{release}
Obsoletes:      %{name}-javadoc < %{version}-%{release}

%description    help
This package contains javadoc for %{name}.

%prep
%autosetup -n commons-vfs-%{version} -p1

%pom_remove_plugin :apache-rat-plugin

sed -i.orig 's|\r||g' LICENSE.txt NOTICE.txt README.txt RELEASE-NOTES.txt
touch -r LICENSE.txt.orig LICENSE.txt
touch -r NOTICE.txt.orig NOTICE.txt
touch -r README.txt.orig README.txt
touch -r RELEASE-NOTES.txt.orig RELEASE-NOTES.txt
rm LICENSE.txt.orig NOTICE.txt.orig README.txt.orig RELEASE-NOTES.txt.orig

%pom_disable_module dist
%pom_change_dep -r :ant org.apache.ant:
%pom_change_dep -r :bcprov-jdk16 :bcprov-jdk15on
%pom_remove_dep -r org.apache.jackrabbit:

rm -rf core/src/{main,test}/java/org/apache/commons/vfs2/provider/webdav

%pom_remove_dep -r :sshd-core

%if %{without hadoop}
%pom_remove_dep -r org.apache.hadoop
rm -r core/src/{main,test}/java/org/apache/commons/vfs2/provider/hdfs
%endif

%pom_remove_plugin :maven-checkstyle-plugin
%pom_remove_plugin :findbugs-maven-plugin

%if %{without ssh}
%pom_remove_dep -r :jsch
rm -r core/src/{main,test}/java/org/apache/commons/vfs2/provider/sftp
rm examples/src/main/java/org/apache/commons/vfs2/libcheck/SftpCheck.java
%endif

%if %{without ftp}
%pom_remove_dep -r :ftpserver-core
rm -r core/src/{main,test}/java/org/apache/commons/vfs2/provider/ftps
%endif


%mvn_file :commons-vfs2 %{name}
%mvn_file :commons-vfs2 %{name}2
%mvn_file :commons-vfs2 commons-vfs
%mvn_file :commons-vfs2 commons-vfs2
%mvn_file :commons-vfs2-examples %{name}-examples
%mvn_file :commons-vfs2-examples %{name}2-examples
%mvn_file :commons-vfs2-examples commons-vfs-examples
%mvn_file :commons-vfs2-examples commons-vfs2-examples

%mvn_alias :commons-vfs2 "org.apache.commons:commons-vfs" "commons-vfs:commons-vfs"
%mvn_alias :commons-vfs2-examples "org.apache.commons:commons-vfs-examples" "commons-vfs:commons-vfs-examples"

%build
%mvn_build -sf

%install
%mvn_install

mkdir -p %{buildroot}%{_sysconfdir}/ant.d
echo "ant commons-logging commons-vfs" > commons-vfs
install -p -m 644 commons-vfs %{buildroot}%{_sysconfdir}/ant.d/commons-vfs

%files          -f .mfiles-commons-vfs2 -f .mfiles-commons-vfs2-project
%doc README.txt RELEASE-NOTES.txt
%license LICENSE.txt NOTICE.txt

%files          devel -f .mfiles-commons-vfs2-examples
%config %{_sysconfdir}/ant.d/commons-vfs
%license NOTICE.txt

%files          help -f .mfiles-javadoc
%license NOTICE.txt

%changelog
* Thu Nov 14 2024 shaojiansong <shaojiansong@kylinos.cn> - 2.1-16
- Fix read return wrong value when reading 0xFF

* Wed Sep 02 2020 chengzihan <chengzihan2@huawei.com> - 2.1-15
- disable hadoop dependance option.

* Tue Mar 03 2020 lihao <lihao129@huawei.com> - 2.1-14
- Package Init
